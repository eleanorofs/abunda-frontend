package data.dtos;

case class GitLabOAuthRequest(clientID: String,
  code: String,
  redirectURI: String,
  codeVerifier: String
);

case class GitLabOAuthResponse(accessToken: String,
  expiresIn: Int,
  refreshToken: String,
  createdAt: Int
);

case class SignIn(username: String, password: String);

case class SignUp(username: String, password: String);

case class Token(token: String);
