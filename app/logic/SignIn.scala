package logic;

import data._;
import logic.validation._;
import scala.util._;
import web._;

object SignIn
{
  val toDTO: models.SignIn => Validated[dtos.SignIn] = (model) =>
  {
    var failureCodes: List[String] = Nil;
    if (model.password.length > 128)
    {
      failureCodes ::= "SIGNIN_PASSWORD_LENGTH";
    }
    if (failureCodes == Nil)
    {
      Valid[dtos.SignIn](dtos.SignIn(model.username, model.password))
    }
    else { Invalid[data.dtos.SignIn](failureCodes) }
  }
}
