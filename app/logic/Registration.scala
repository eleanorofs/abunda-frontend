package logic;

import data._;
import logic.validation._;
import scala.util._;
import scala.util.matching.Regex;
import web._;

object Registration
{
  val toDTO: models.Registration => Validated[dtos.SignUp] =
    (model) =>
  {
    var failureCodes: List[String] = Nil;
    if(!model.agreeToTerms)
    {
      failureCodes ::= "REGISTRATION_AGREEMENT";
    }
    if (model.password.length() < 16)
    {
      failureCodes ::= "REGISTRATION_PASSWORD_SHORT";
    }
    if (model.password.length() > 128)
    {
      failureCodes ::= "REGISTRATION_PASSWORD_LONG";
    }
    if (model.password != model.confirmPassword)
    {
      failureCodes ::= "REGISTRATION_PASSWORD_CONFIRM";
    }
    if (model.username.length() < 1)
    {
      failureCodes ::= "REGISTRATION_USERNAME_SHORT";
    }
    if (model.username.matches("[\\w-]"))
    {
      failureCodes ::= "REGISTRATION_USERNAME_IRREGULAR"
    }
    if (failureCodes == Nil)
    {
      Valid[dtos.SignUp](dtos.SignUp(model.username, model.password))
    }
    else { Invalid[data.dtos.SignUp](failureCodes) }
  };
}
