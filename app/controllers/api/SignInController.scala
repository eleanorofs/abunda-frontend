package controllers.api;

import data._;
import data.dtos.SignUp;
import javax.inject._;
import logic.Registration;
import play.api._;
import play.api.libs.json._;
import play.api.libs.ws._;
import play.api.mvc._;
import scala.concurrent.ExecutionContext;
import scala.concurrent._;
import scala.util.chaining._;
import web._;
import web.cookie.HgSessionCookie;
import web.resultMonad;
import web.resultMonad.ResultMonad;
import web.models._;

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class SignInController @Inject()
  (configuration: Configuration,
    val controllerComponents: ControllerComponents,
    ws: WSClient) extends BaseController
{
  implicit val ec: ExecutionContext = ExecutionContext.global;

  def act = Action.async(parse.tolerantJson)
  {
    request: Request[JsValue] =>
    {
      web.parsing.SignIn.toModel(request.body) map
      (model => logic.SignIn.toDTO(model)) flatMap
      (validated => ResultMonad.fromValidated(validated)) map
      (dto => rest(dto)) pipe
      ResultMonad.invertFuture map
      (rm => rm.flatMap(rm => rm)) map
      (monad => monad map (session => okSignedIn(session))) map
      (monad => monad toResult (ok => ok))
    }
  };

  /*--------------------------helpers-------------------------*/
  private def dbConfig =
    DatabaseConfiguration(configuration.get[String]("postgrestUrl"), ws);

  private def rest(dto: dtos.SignIn): Future[ResultMonad[models.Session]] =
  {
    Database.signIn(dbConfig, dto) map
    (resp => ResultMonad.fromWSResponse(resp)) map
    (monad => monad map (json =>
      json.validate[dtos.Token](Json.reads[dtos.Token]))) map
    (monad => monad map (result => ResultMonad.fromJsResult(result))) map
    (monad => monad flatMap (monad => monad)) map
    (monad => monad map (token => models.Session(dto.username, token.token)))
  }

  private def okSignedIn(session: models.Session): Result =
  {
    Conf.getInstance(configuration)
      .pipe(conf => ClientSession.fromConf(conf))
      .pipe(cs => ClientSession.signIn(cs, session.username))
      .pipe(session => ClientSession.toJson(session))
      .pipe(json => Ok(json))
      .withSession(HgSessionCookie.signIn(session.token, session.username));
  };
}
