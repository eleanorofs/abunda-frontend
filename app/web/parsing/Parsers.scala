package web.parsing;

import data.dtos;
import play.api.libs.json._;
import play.api.libs.functional.syntax._;
import web.models;

object Parsers
{
  val registration: Reads[models.Registration] =
    (
      (JsPath \ "username").read[String] and
        (JsPath \ "password").read[String] and
        (JsPath \ "confirmPassword").read[String] and
        (JsPath \ "agreeToTerms").read[Boolean]
    )(models.Registration.apply _)

  val token: Reads[String] = (JsPath \ "token").read[String];
}
