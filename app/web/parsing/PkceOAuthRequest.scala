package web.parsing;

import play.api.libs.json._;
import scala.util.chaining._;
import web.resultMonad.ResultMonad;
import web.parsing.Parsers;

object PkceOAuthRequest
{
  def toModel(jsValue: JsValue): ResultMonad[web.models.PkceOAuthRequest] =
  {
    (jsValue.validate(Json.reads[web.models.PkceOAuthRequest]))
      .pipe(jsResult => ResultMonad.fromJsResult(jsResult))
  };

  def toJson(model: web.models.PkceOAuthRequest): String =
  {
    implicit val writes = Json.writes[web.models.PkceOAuthRequest];
    Json.toJson(model)(writes).toString();
  };
}
