package web.parsing;

import play.api.libs.json._;
import scala.util.chaining._;
import web.resultMonad.ResultMonad;
import web.parsing.Parsers;

object State
{
  def toModel(jsValue: JsValue): ResultMonad[web.models.State] =
  {
    (jsValue.validate[web.models.State](Json.reads[web.models.State]))
      .pipe(jsResult => ResultMonad.fromJsResult(jsResult))
  };
}
