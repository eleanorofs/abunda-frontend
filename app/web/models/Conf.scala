package web.models;

final case class Conf
(
  gitLabClientId: String,
  gitLabRedirectUrl: String,
  postgrestUrl: String
);

final object Conf
{
  def getInstance(configuration: play.api.Configuration): Conf =
  {
    Conf(
      gitLabClientId=configuration.get[String]("gitLab.clientId"),
      gitLabRedirectUrl = configuration.get[String]("gitLab.redirectUrl"),
      postgrestUrl=configuration.get[String]("postgrestUrl")
    );
  };
};
