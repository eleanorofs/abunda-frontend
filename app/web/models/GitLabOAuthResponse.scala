package web.models;

import play.api.mvc.Session;

final case class GitLabOAuthResponse(accessToken: String,
  expiresIn: Int,
  refreshToken: String,
  createdAt: Int
);

object GitLabOAuthResponse
{
  def accessTokenKey = "gitLabOAuthResponse.accessToken";
  def expiresInKey = "gitLabOAuthResponse.expiresIn";
  def refreshTokenKey = "gitLabOAuthResponse.refreshToken";
  def createdAtKey = "gitLabOAuthResponse.createdAtkey";
  def fromSession(sess: play.api.mvc.Session): Option[GitLabOAuthResponse] =
  {
    sess.get(accessTokenKey)
      .map(at => GitLabOAuthResponse(at, 0, "", 0))
      .flatMap(resp => getInt(sess, expiresInKey)
        .map(exp => resp.copy(expiresIn=exp)))
      .flatMap(resp => sess.get(refreshTokenKey)
        .map(refr => resp.copy(refreshToken=refr)))
      .flatMap(resp => getInt(sess, createdAtKey)
        .map(createdAt => resp.copy(createdAt=createdAt)));
  };

  /*-------------------------------------------------------------------------*/
  private def getInt(session: play.api.mvc.Session, key: String): Option[Int] =
  {
    session.get(key).flatMap(str => {
      try { Some(str.toInt) }
      catch { case e: Exception => None }
    });
  }
}
